from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import authenticate

urlpatterns = [
    # Examples:
    # url(r'^$', 'fpp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^$', 'fpp.views.home', name='home'),
    url(r'^', include('fpp_archive.urls', namespace="fpp_archive")),
    url(r'^courses/', include('courses.urls', namespace="courses")),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url('^', include('django.contrib.auth.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

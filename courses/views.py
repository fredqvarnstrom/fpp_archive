from io import BytesIO

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.db.models import Count
from django.conf import settings
#from django.contrib.auth.forms import UserCreationForm
from .forms import RegistrationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from bs4 import BeautifulSoup
from ebooklib import epub

from .models import Course, Lesson


# Create your views here.
def courses(request):
    '''Render the Request Page.'''
    return render(request, 'courses/index.html')

@login_required
def toc(request, course_name):
    course = get_object_or_404(Course, slug=course_name)
    lessons = course.lesson_set.all()
    sections = lessons.values('section_number')\
        .order_by('section_number')\
        .annotate(total=Count('section_number'))
    lessons_final = []

    index = 0
    old_value = 1
    for section in sections:
        # Calculate the starting and ending day of each section
        start = old_value
        finish = start + (section['total'] - 1)
        solo = True if (finish - start == 0) else False
        old_value = finish + 1

        # Build a list for looping through sections and lessons
        lessons_final.append({})
        lessons_final[index]['section_number'] = section['section_number']
        lessons_final[index]['lessons'] = lessons.filter(section_number=section['section_number'])
        lessons_final[index]['total'] = section['total']
        lessons_final[index]['start'] = start
        lessons_final[index]['finish'] = finish
        lessons_final[index]['solo'] = solo
        index += 1

    context = {
        'course': course,
        'lessons': lessons_final
    }
    return render(request, 'courses/toc.html', context)

@login_required
def single(request, course_name, course_day):
    course = get_object_or_404(Course, slug=course_name)
    lesson = get_object_or_404(Lesson, \
                               course__slug=course_name, \
                               day_number=course_day)
    total_days = course.lesson_set.all().count()
    next_day = (lesson.day_number + 1) if (lesson.day_number < total_days) else False

    context = {
        'course': course,
        'lesson': lesson,
        'next': next_day,
    }
    return render(request, 'courses/single.html', context)

def epub_gen(request, filetype, course_name):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/epub+zip')
    response['Content-Disposition'] = 'attachment; filename="' + course_name + '.epub"'

    buffer = BytesIO()

    course = get_object_or_404(Course, slug=course_name)
    lessons = course.lesson_set.all()
    sections = lessons.values('section_number') \
        .order_by('section_number') \
        .annotate(total=Count('section_number'))
    sect_list = []

    index = 0
    for section in sections:
        # Build a list for looping through sections and lessons
        sect_list.append({})
        sect_list[index]['section_number'] = section['section_number']
        sect_list[index]['lessons'] = lessons.filter(section_number=section['section_number'])
        index += 1

    book = epub.EpubBook()

    # set metadata
    book.set_identifier('id123456')
    book.set_title(course.title)
    book.set_language('en')

    book.add_author('First Principles Press')
    book.toc = []
    book.spine = ['nav']

    # create chapter
    index = 0
    for section in sect_list:
        book.toc.append([epub.Section('Section ' + str(section['section_number'])), []])
        for lesson in section['lessons']:
            soup = BeautifulSoup(lesson.content, 'html.parser')
            if soup.img:
                soup.img.decompose()
            file_name = 'day_' + str(lesson.day_number) + '.xhtml'
            chapter = epub.EpubHtml(title=lesson.title, file_name=file_name)
            chapter.content = '<h1>' + lesson.title + '</h1>'
            chapter.content += str(soup)
            book.add_item(chapter)
            book.toc[index][1].append(chapter)
            book.spine.append(chapter)
        index += 1

    # add default NCX and Nav file
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    # define CSS style
    style = 'BODY {color: white;}'
    nav_css = epub.EpubItem(uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style)

    # add CSS file
    book.add_item(nav_css)

    # write to the file
    epub.write_epub(buffer, book, {})


    # Get the value of the BytesIO buffer and write it to the response.
    epub_file = buffer.getvalue()
    buffer.close()
    response.write(epub_file)
    return response

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password2'])
            login(request, user)
            return redirect(form.cleaned_data['next'])
    else:
        context = {
            'form': RegistrationForm(),
            'next': request.GET.get('next'),
        }
        return render(request, 'courses/register.html', context)

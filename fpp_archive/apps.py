from django.apps import AppConfig

class FppArchiveConfig(AppConfig):
    name = 'fpp_archive'
    verbose_name = 'FPP Archive'

    def ready(self):
        from . import signals

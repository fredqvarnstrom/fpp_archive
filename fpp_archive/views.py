'''Views for the FPP Archive.'''
import os
import pickle
from smtplib import SMTPException

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import EmailMessage, send_mail

from .whoosh_search import FppIndex
from fpp_archive.models import Document
from fpp_archive.ocrPdf import Converter
from fpp.settings import MEDIA_ROOT, MEDIA_URL
from django.contrib import messages

def home(request):
    '''Render the homepage.'''
    context = {'status': None}
    if request.GET.__contains__('status'):
        context['status'] = request.GET.get('status')

    return render(request, 'fpp_archive/home.html', context)

def categories(request):
    '''Render the FPP categories pages.'''
    return render(request, 'fpp_archive/categories.html')

def search_results(request):
    '''Parse a user query, and return a paginated list of results.'''
    if request.GET.__contains__('q'):
        query = request.GET.get('q')
        fulltext = request.GET.get('fulltext')

        temp_index = FppIndex('indexdir')
        full_results = temp_index.search(query.lower(), fulltext)
        paginator = Paginator(full_results, 12) # Show 12 results per page

        page = request.GET.get('page')
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            page = 1
            results = paginator.page(page)
        except EmptyPage:
            # If page is out of range, deliver the last page of results
            page = paginator.num_pages
            results = paginator.page(page)

        first_doc = int(page) * 12 - 11
        last_doc = int(page) * 12
        if last_doc > int(paginator.count):
            last_doc = paginator.count

        if int(page) - 3 < 0:
            page_range = paginator.page_range[0:5]
        else:
            page_range = paginator.page_range[int(page)-3:int(page)+2]

        print(page_range)

        context = {
            'query':     request.GET.get('q'),
            'results':   results,
            'total':     paginator.count,
            'page_range': page_range,
            'first_doc': first_doc,
            'last_doc':  last_doc,
            'full_text': fulltext
        }
        return render(request, 'fpp_archive/search_results.html', context)
    elif request.GET.__contains__('cat'):
        category = request.GET.get('cat')

        temp_index = FppIndex('indexdir')
        full_results = temp_index.cat_search(category)
        paginator = Paginator(full_results, 12) # Show 12 results per page

        page = request.GET.get('page')
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            page = 1
            results = paginator.page(page)
        except EmptyPage:
            # If page is out of range, deliver the last page of results
            page = paginator.num_pages
            results = paginator.page(page)

        first_doc = int(page) * 12 - 11
        last_doc = int(page) * 12
        if last_doc > int(paginator.count):
            last_doc = paginator.count

        if int(page) - 3 < 0:
            page_range = paginator.page_range[0:5]
        else:
            page_range = paginator.page_range[int(page)-3:int(page)+2]

        print(page_range)

        context = {
            'cat':     request.GET.get('cat'),
            'results':   results,
            'total':     paginator.count,
            'page_range': page_range,
            'first_doc': first_doc,
            'last_doc':  last_doc
        }
        return render(request, 'fpp_archive/search_results.html', context)
    else:
        return HttpResponseRedirect('/')

def contact(request):
    '''Send user message to FPP, redirect to / with success message or
    complain on failure.'''
    if request.method == 'POST':
        subject = '[FPP Archive] Message from %s' % request.POST['name']
        body = 'From:\n{0}\n{1}\n\nMessage:\n{2}'.format(
            request.POST['name'],
            request.POST['email'],
            request.POST['comments']
        )
        from_email = request.POST['email']
        recipient_list = ['bigideamgroup@gmail.com']
        try:
            send_mail(subject,
                      body,
                      from_email,
                      recipient_list,
                      fail_silently=True)
            messages.add_message(request, messages.SUCCESS, 'Message Sucessfully Sent!')
            return HttpResponseRedirect('/')
        except SMTPException:
            messages.add_message(request, messages.ERROR, 'Message Failed To Send!')
            return HttpResponseRedirect('/')
    return HttpResponseRedirect('/')

def upload(request):
    '''Send user-contributed file to FPP, redirect to / with success message
    or complain on failure.'''
    if request.method == 'POST':
        subject = '[FPP Archive] User Contribution'
        body = 'An FPP Archive user submitted the attached file.'
        from_email = 'will@quexxon.net'
        recipient_list = ['bigideamgroup@gmail.com']
        attach = request.FILES['upload']
        email = EmailMessage(subject, body, from_email, recipient_list)
        email.attach(attach.name, attach.read(), attach.content_type)
        try:
            email.send(fail_silently=False)
            messages.add_message(request, messages.SUCCESS, 'Document \
                                 Sucessfully Submited For Review!')
            return HttpResponseRedirect('/')
        except SMTPException:
            messages.add_message(request, messages.SUCCESS, 'Failed To Submit Document!')
            return HttpResponseRedirect('/')
    return HttpResponseRedirect('/')

def verify_upload(request):
    '''Adds and Removes database document queue to search index.'''
    if request.method == 'POST':
        idx = FppIndex('indexdir')
        index_queue_file = MEDIA_ROOT + 'temp/index_queue'
        index_remove_file = MEDIA_ROOT + 'temp/remove_queue'
        try:
            if os.path.isfile(index_queue_file):
                idx.create()
                with open(index_queue_file, 'rb') as temp:
                    index_queue = pickle.load(temp)

                for index in index_queue:

                    instance = Document.objects.get(id=index)
                    keyword_tuples = instance.keywords.values_list()
                    temp_keyword_list = many_to_many_field_iter(keyword_tuples)
                    keyword_list = ""
                    for item in temp_keyword_list:
                        keyword_list += item + ','
                    keyword_list = keyword_list[:-1]
                    category_tuples = instance.categories.values_list()
                    temp_category_list = many_to_many_field_iter(category_tuples)
                    category_list = ""
                    for item in temp_category_list:
                        category_list += item + ','
                    category_list = category_list[:-1]
                    file_path = MEDIA_ROOT + str(instance.file)
                    file_url = MEDIA_URL + str(instance.file)
                    img_path = MEDIA_ROOT + "temp/img/"
                    txt_path = MEDIA_ROOT + "temp/txt/"
                    converter = Converter(file_path, img_path, txt_path)
                    content = converter.convert()
                    idx.add(doc_id=str(index),
                            title=instance.title.lower(),
                            url=file_url,
                            content=content.lower(),
                            author=instance.author.lower(),
                            publication=instance.publication.lower(),
                            pub_date=instance.publication_date.lower(),
                            keyword=keyword_list.lower(),
                            category=category_list.lower())

                os.remove(index_queue_file)
                messages.add_message(request, messages.SUCCESS, 'File Sucessfully Added!')
            else:
                messages.add_message(request, messages.WARNING, 'No files to add.')
        except IOError:
            messages.add_message(request, messages.ERROR, 'Sync Failed!')
            return HttpResponseRedirect('/admin/')
        try:
            if os.path.isfile(index_remove_file):
                idx.create()
                with open(index_remove_file, 'rb') as remove_temp:
                    remove_queue  = pickle.load(remove_temp)

                for index in remove_queue:
                    idx.delete('doc_id', str(index))

                os.remove(index_remove_file)
                messages.add_message(request, messages.SUCCESS, 'Files Sucessfully Removed!')
            else:
                messages.add_message(request, messages.WARNING, 'No files to remove.')
        except IOError:
            messages.add_message(request, messages.ERROR, 'Sync Failed!')
            return HttpResponseRedirect('/admin/')

        return HttpResponseRedirect('/admin/')

    return HttpResponseRedirect('/admin/')


def many_to_many_field_iter(tuple_list):
    '''Yield the second value of each tuple in a list.'''
    for item in tuple_list:
        yield item[1]


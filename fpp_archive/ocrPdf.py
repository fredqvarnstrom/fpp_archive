from subprocess import call
from pathlib import Path
from os import path, remove
from random import random
from PIL import Image

try:
    from pytesseract import image_to_string
except ImportError:
    pass


class Converter:
    def __init__(self, input_file, img_path, txt_path):
        self.input_file = input_file
        self.input_file_base = path.basename(input_file)
        self.img_path = Path(img_path)
        self.txt_path = Path(txt_path)

    def convert(self):
        temp_img_path = str(self.img_path) + '/img'
        call(['pdfimages', '-png', self.input_file, temp_img_path])

        img_list_temp = self.img_path.glob('*.png')
        img_list = []

        for img in img_list_temp:
            img_list.append(str(img))

        img_list.sort()

        txt_file_base = path.join(str(self.txt_path), self.input_file_base[:-4])
        txt_file = txt_file_base + '.txt'

        with open(txt_file, 'w') as temp_file:
            for img in img_list:
                #call(['../textcleaner.sh', img, img])
                ocr_text = image_to_string(Image.open(img))
                print(ocr_text)
                temp_file.write(ocr_text)
                remove(img)

        with open(txt_file, 'r') as f:
            ocr_text = f.read()
            return ocr_text


if __name__ == '__main__':
    from sys import argv
    try:
        converter = Converter(argv[1], argv[2], argv[3])
        converter.convert()
    except IndexError:
        print('Invalid arguments!')

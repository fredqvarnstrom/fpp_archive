from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^results/$', views.search_results, name='results'),
    url(r'^contact-us/$', views.contact, name='contact'),
    url(r'^upload/$', views.upload, name='upload'),
    url(r'^verify/$', views.verify_upload, name='verify_upload'),
    url(r'^categories/$', views.categories, name='categories'),
]

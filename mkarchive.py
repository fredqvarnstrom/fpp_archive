#!/usr/bin/env python3
'''Create a temporary index from the existing FPP datastore.'''

from pathlib import Path

from fpp_archive.whoosh_search import FppIndex

KEYWORD_LIST = ['501(c)3',
                'Aaron Brown',
                'ACLU',
                'Adam Weishaupt',
                'Al Gore',
                'Alexander Hamilton',
                'Alger Hiss',
                'Alphonso Taft',
                'American Law',
                'American Revolution',
                'Antony Sutton',
                'AOL Time Warner',
                'Arnold Toynbee',
                'Atheism',
                'Atlantic Monthly',
                'Bain investments',
                'Barron’s Magazine',
                'Barry Goldwater',
                'Beardsley Ruml',
                'Ben Bagdikian',
                'Benjamin Franklin',
                'Bernard Goldberg',
                'Bertelsmann',
                'Bertrand Russsell',
                'Bible',
                'Biblical Principles',
                'Bill Gates',
                'Bill Gertz',
                'Bill of Rights',
                'Blumenfeld',
                'Bonesmen',
                'Carroll Quigley',
                'Catherine Barrett',
                'Cecil Rhodes',
                'Charles F. Potter',
                'Chicago Tribune',
                'China',
                'Christian Nation',
                'Christianity',
                'Church of the Holy Trinity vs. United States.',
                'Colonel Edward Mandell House',
                'Commentaries on the Laws of England',
                'Communism',
                'Congress',
                'Constitution',
                'Copenhagen',
                'Council on Foreign Relations',
                'Dan Smoot',
                'Daniel Boorstin',
                'Darwin',
                'David Barton',
                'David Rockefeller',
                'Debt',
                'Deception',
                'Declaration of Independence',
                'Dennis L. Cuddy',
                'Disney',
                'Duncan Hunter',
                'Edward Thorndike',
                'Eli Lilly',
                'Ellen Hodgson Brown',
                'FBI',
                'Fisher Ames',
                'Founders',
                'Frances Wright l',
                'Frankfurter',
                'Free Enterprise',
                'Freedom of worship',
                'G. Stanley Hall',
                'G. W. Snyder',
                'Gary Graham',
                'Gary T. Amos',
                'General Education Board',
                'General Electric',
                'George W. McKinney Jr.',
                'George Washington',
                'Goldman Sachs',
                'Herbert Hoover',
                'Humanism',
                'Idi Amin',
                'Isaiah',
                'J. Edgar Hoover',
                'J.P. Morgan',
                'Jackson Turner Main',
                'James Cattell',
                'James Fallows',
                'James Knight',
                'James Madison',
                'James Perloff',
                'Jeremiah',
                'Jesus Christ',
                'Jimmy Carter',
                'John D. Rockefeller',
                'John Dewey',
                'John Hylan',
                'John Jay',
                'John Mitchell Mason',
                'John Quincy Adams',
                'Jon Katz',
                'Jonah Goldberg',
                'Joseph Farrah',
                'Josiah David Brewer',
                'Joy Elmer Morgan',
                'Knight Foundation',
                'Kris Millegan',
                'KRS 158.195',
                'Law Enforcement Bulletin',
                'League for Industrial Democracy',
                'Leipzig',
                'Leviticus',
                'Lewis Rukeyser',
                'Libertarianism',
                'Limited Government',
                'Lindisfarne Center',
                'Lucifer Publishing Company',
                'Lucis Trust',
                'Madame Blavatsky',
                'Marriner Stoddard Eccles',
                'Masonic',
                'Matt Drudge',
                'Maurice Greenburg',
                'Maynard Institute',
                'McCormick Foundation',
                'Media Monopoly',
                'Miami Herald',
                'Micah',
                'Microsoft',
                'Mikhail Gorbachev',
                'Mitt Romney',
                'Moral Self Government',
                'Murdoch',
                'National Education Association',
                'National Museum of History',
                'New Age',
                'New York Times',
                'News Corporation',
                'Old Testament',
                'Ordered Liberty',
                'Paolo Lionni',
                'Patrick Henry',
                'Pentagon',
                'Peter Allison',
                'Philip Hamburger',
                'Philip Vander Velde',
                'Reece Committee',
                'Rehnquist',
                'Representatives',
                'Revolutionary',
                'Robert Dale Owen',
                'Rockefeller',
                'Roy Moore',
                'Rule of Law',
                'Samuel Blumenfeld',
                'Sanger',
                'Smithsonian',
                'Spiro Agnew',
                'Stalin',
                'Standard Oil',
                'Statism',
                'Stephen McDowell',
                'Steve Hallman',
                'Suppression of History',
                'Supreme Court',
                'Tax Exempt Foundations',
                'The Federal Reserve:',
                'The Federalist Papers',
                'The Illuminati',
                'Theocracy',
                'Thomas Jefferson',
                'Uganda',
                'UNESCO',
                'United Nations',
                'United States of America',
                'Viacom',
                'Vietnam',
                'Washington Times',
                'Webster',
                'White House Chief of Staff',
                'Wilhelm Wundt',
                'William A. Rusher',
                'William Blackstone',
                'William E. Kennard',
                'William F. Jasper',
                'William Howard Taft',
                'William Huntington Russell',
                'William Irwin Thompson',
                'William Norman Grigg',
                'Woodrow Wilson',
                'World Health Organization',
                'World War II',
                'WorldNetDaily',
                'Zbigniew Brzezinski']

def main():
    '''Main loop.'''
    index = FppIndex('indexdir')
    txt_path = Path('txt')
    txt_list = list(txt_path.glob('**/*.txt'))

    # Metadata defaults
    author = None
    publication = None
    pub_date = None
    title = None

    index.create()

    doc_id = 200000000
    for txt_file in txt_list:
        url = '/media/documents/us_military/' + txt_file.name[:-3] + 'pdf'
        metadata = txt_file.name[:-4].lower().split(sep='_')

        if len(metadata) != 4:
            print(metadata)

        if len(metadata) >= 1 and metadata[0]:
            title = metadata[0]
        if len(metadata) >= 2 and metadata[1] not in ['no author', 'noauthor']:
            author = metadata[1]
        if len(metadata) >= 3 and metadata[2] not in ['no date', 'nodate']:
            pub_date = metadata[2]
        if len(metadata) >= 4 and metadata[3] not in ['no publisher', 'nopublisher']:
            publication = metadata[3]

        with txt_file.open() as file_handle:
            keyword = ''
            content = file_handle.read()
            for word in KEYWORD_LIST:
                if word in content:
                    keyword += word + ','

        index.add(doc_id=str(doc_id),
                  title=title,
                  url=url,
                  content=content,
                  author=author,
                  publication=publication,
                  pub_date=pub_date,
                  keyword=keyword[:-1],
                  category='U.S. Military')

        doc_id += 1

if __name__ == '__main__':
    main()
